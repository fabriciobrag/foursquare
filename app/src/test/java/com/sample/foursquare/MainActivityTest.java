package com.sample.foursquare;

import com.sample.foursquare.models.Item;
import com.sample.foursquare.network.FourSquareApiInterface;
import com.sample.foursquare.ui.activities.MainActivity;
import com.sample.foursquare.viewmodel.MainActivityView;
import com.sample.foursquare.viewmodel.MainActivityViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
public class MainActivityTest {

    @Rule RxSchedulersOverrideRule rxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    @Mock MainActivityView mainActivityView;

    private MainActivityViewModel mainActivityViewModel;

    @Mock MainActivity mainActivity;

    @Mock FourSquareApiInterface fourSquareApiInterface;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mainActivityViewModel = new MainActivityViewModel(fourSquareApiInterface);
        mainActivityViewModel.attach(mainActivityView);
    }

    @Test
    public void loadVenue() {

        List<Item> venueList = new ArrayList<>();
        venueList.add(new Item());

        String ll = "40.7,-74";

        doReturn(Observable.just(venueList)).when(fourSquareApiInterface).getVenues(ll,
                BuildConfig.OAUTH_TOKEN, BuildConfig.VERSION_PARAM);

        mainActivityViewModel.fetchVenues(ll);
        verify(mainActivityView, times(1)).load(venueList);

    }

}

