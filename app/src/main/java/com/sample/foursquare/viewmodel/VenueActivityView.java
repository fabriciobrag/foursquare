package com.sample.foursquare.viewmodel;

/**
 * Used for interaction between ViewModel and Activities/Fragments
 */
public interface VenueActivityView extends IView {
    void load(String venueId);
}
