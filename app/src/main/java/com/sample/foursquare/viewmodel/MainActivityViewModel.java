package com.sample.foursquare.viewmodel;


import com.sample.foursquare.BuildConfig;
import com.sample.foursquare.network.FourSquareApiInterface;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * ViewModel for MainActivity
 */

public class MainActivityViewModel extends BaseViewModel<MainActivityView> {

    private FourSquareApiInterface fourSquareApi;

    public MainActivityViewModel(FourSquareApiInterface fourSquareApi) {
        this.fourSquareApi = fourSquareApi;
    }

    public void fetchVenues(String ll) {

        compositeDisposable.add(fourSquareApi.getVenues(ll, BuildConfig.OAUTH_TOKEN, BuildConfig.VERSION_PARAM)
                .observeOn(AndroidSchedulers.mainThread())
                .map(response -> response.getResponse().getGroups().get(0).getItems())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> view.load(response), throwable -> view.error(throwable)));

    }
}
