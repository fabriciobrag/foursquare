package com.sample.foursquare.viewmodel;

import com.sample.foursquare.models.Item;

import java.util.List;

/**
 * Used for interaction between ViewModel and Activities/Fragments
 */
public interface MainActivityView extends IView {
    void load(List<Item> item);
}
