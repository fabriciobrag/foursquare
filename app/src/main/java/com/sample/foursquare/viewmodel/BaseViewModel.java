package com.sample.foursquare.viewmodel;

import io.reactivex.disposables.CompositeDisposable;

/**
 * BaseViewModel
 * Created by fabricio on 22/04/2017.
 */
public class BaseViewModel<T extends IView> {

    CompositeDisposable compositeDisposable;
    T view;

    BaseViewModel() {
        compositeDisposable = new CompositeDisposable();
    }

    public void attach(T view) {
        this.view = view;
    }

    public void detach() {
        view = null;
    }

    public void clearSubscriptions() {
        compositeDisposable.clear();
    }

}
