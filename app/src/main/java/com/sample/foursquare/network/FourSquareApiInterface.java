package com.sample.foursquare.network;

import com.sample.foursquare.models.Venues;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface FourSquareApiInterface {

    @GET("/v2/venues/explore")
    Observable<Venues> getVenues(@Query("ll") String ll,
                                 @Query("oauth_token") String oauthToken,
                                 @Query("v") String version);

}
