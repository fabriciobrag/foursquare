package com.sample.foursquare.di.modules;

import com.sample.foursquare.di.scopes.UserScope;
import com.sample.foursquare.network.FourSquareApiInterface;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class FourSquareModule {

    @Provides
    @UserScope
    FourSquareApiInterface providesFourSquareApiInterface(Retrofit retrofit) {
        return retrofit.create(FourSquareApiInterface.class);
    }
}
