package com.sample.foursquare.di.components;

import com.sample.foursquare.di.modules.FourSquareModule;
import com.sample.foursquare.di.modules.GoogleApiModule;
import com.sample.foursquare.di.scopes.UserScope;
import com.sample.foursquare.ui.activities.MainActivity;

import dagger.Component;

@UserScope
@Component(dependencies = NetComponent.class, modules = {FourSquareModule.class, GoogleApiModule.class}  )
public interface FourSquareComponent {
    void inject(MainActivity activity);
}
