package com.sample.foursquare.di.modules;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.sample.foursquare.di.scopes.UserScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by fabricio on 23/04/2017.
 */
@Module
public class GoogleApiModule {

    private Context mContext;

    public GoogleApiModule(Context context) {
        this.mContext = context;
    }

    @Provides
    @UserScope
    GoogleApiClient providesGoogleApiClient() {
        return new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .build();
    }
}
