package com.sample.foursquare;

import android.app.Application;

import com.sample.foursquare.di.components.DaggerFourSquareComponent;
import com.sample.foursquare.di.components.DaggerNetComponent;
import com.sample.foursquare.di.components.FourSquareComponent;
import com.sample.foursquare.di.components.NetComponent;
import com.sample.foursquare.di.modules.AppModule;
import com.sample.foursquare.di.modules.FourSquareModule;
import com.sample.foursquare.di.modules.GoogleApiModule;
import com.sample.foursquare.di.modules.NetModule;

/**
 *
 * Created by fabricio on 22/04/2017.
 */

public class MyApp extends Application {

    private NetComponent mNetComponent;
    private FourSquareComponent mFourSquareComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // specify the full namespace of the component
        // Dagger_xxxx (where xxxx = component name)
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.END_POINT))
                .build();

        mFourSquareComponent = DaggerFourSquareComponent.builder()
                .netComponent(mNetComponent)
                .googleApiModule(new GoogleApiModule(this))
                .fourSquareModule(new FourSquareModule())
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    public FourSquareComponent getFourSquareComponent() {
        return mFourSquareComponent;
    }
}