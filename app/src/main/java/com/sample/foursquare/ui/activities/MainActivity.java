package com.sample.foursquare.ui.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sample.foursquare.MyApp;
import com.sample.foursquare.R;
import com.sample.foursquare.databinding.ActivityMainBinding;
import com.sample.foursquare.models.Item;
import com.sample.foursquare.network.FourSquareApiInterface;
import com.sample.foursquare.ui.VenueAdapter;
import com.sample.foursquare.viewmodel.MainActivityView;
import com.sample.foursquare.viewmodel.MainActivityViewModel;

import java.util.List;

import javax.inject.Inject;

/**
 * MainActivity
 * Created by fabricio on 22/04/2017.
 */
public class MainActivity extends BaseActivity<ActivityMainBinding, MainActivityViewModel>
        implements MainActivityView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_LOCATION = 3;

    @Inject FourSquareApiInterface mFourSquareApiInterface;

    @Inject GoogleApiClient mGoogleApiClient;

    private VenueAdapter mVenueAdapter;
    private Location myLocation;

    //sample location
    String ll = "40.7,-74";

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inject components
        ((MyApp) getApplication()).getFourSquareComponent().inject(this);

        viewModel = new MainActivityViewModel(mFourSquareApiInterface);
        viewModel.attach(this);

        bindView(R.layout.activity_main);

        binding.setIsLoading(true);

        mVenueAdapter = new VenueAdapter();
        binding.recyclerView.setAdapter(mVenueAdapter);


        if (mGoogleApiClient != null) {
            mGoogleApiClient.registerConnectionCallbacks(this);
            mGoogleApiClient.registerConnectionFailedListener(this);
        }
    }

    @Override protected void onStart() {
        mGoogleApiClient.connect();

        super.onStart();
    }

    @Override protected void onStop() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override public void onConnected(@Nullable Bundle bundle) {
        getLocation();
    }


    @Override public void onConnectionSuspended(int i) { }

    @Override public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        //load a sample location
        viewModel.fetchVenues(ll);
    }

    @Override public void load(List<Item> item) {
        binding.setIsLoading(false);
        mVenueAdapter.setData(item);
    }

    private void getLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Check Permissions
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION);

        } else { // permission has been granted

            // try to get last location
            myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (myLocation != null) {

                //found location, fetch venues
                String ll = String.format("%s,%s", String.valueOf(myLocation.getLatitude()),
                        String.valueOf(myLocation.getLongitude()));
                viewModel.fetchVenues(ll);

            } else {

                //load a sample location, just until new location is ready or if location service is active
                viewModel.fetchVenues(ll);

                Toast.makeText(this,
                        String.format("Sample location %s \ntry to turn on location service", ll),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                getLocation();

            } else { // Permission was denied or request was cancelled

                //load a sample location when permission denied
                viewModel.fetchVenues(ll);
            }
        }
    }


}
