package com.sample.foursquare.ui;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Custom databinding adapters
 * Created by fabricio on 22/04/2017.
 */

public class CustomBindingAdapters {

    @BindingAdapter({"venue"})
    public static void loadImage(ImageView view, String photo) {

        //can't load foursquare photos
        //@todo replace foursquare photos
        String url = "https://lh3.googleusercontent.com/81tvpT59weJbOGWT9jQ8_9RtcGXKCcVv59BU7Wl6PnS7okIgrS4iTCgwWpPQY2FRKw=w300";
        Picasso.with(view.getContext())
                .load(url)
                .into(view);
    }
}
