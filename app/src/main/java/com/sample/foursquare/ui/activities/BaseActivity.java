package com.sample.foursquare.ui.activities;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.sample.foursquare.viewmodel.BaseViewModel;
import com.sample.foursquare.viewmodel.IView;

/**
 * BaseActivity
 *
 * Created by fabricio on 22/04/2017.
 */

public abstract class BaseActivity<B extends ViewDataBinding, T extends BaseViewModel>
        extends AppCompatActivity implements IView {

    protected T viewModel;
    B binding;

    /**
     * ViewModel must be initialized before bindView() is called
     * @param layout layout for the activity
     */
    protected final void bindView(int layout) {
        if (viewModel == null) {
            throw new IllegalStateException("viewModel must not be null");
        }
        binding = DataBindingUtil.setContentView(this, layout);
    }

    @Override protected void onStop() {
        super.onStop();
        viewModel.clearSubscriptions();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        viewModel.detach();
    }

    @Override public void error(Throwable e) {
        Log.e(viewModel.getClass().getName(), e.getMessage());
        Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
    }

}

