package com.sample.foursquare.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;

import com.sample.foursquare.BuildConfig;
import com.sample.foursquare.R;
import com.sample.foursquare.databinding.ActivityVenueBinding;
import com.sample.foursquare.viewmodel.VenueActivityView;
import com.sample.foursquare.viewmodel.VenueActivityViewModel;

public class VenueActivity extends BaseActivity<ActivityVenueBinding, VenueActivityViewModel>
    implements VenueActivityView {

    public static final String EXTRA_VENUE_ID = "com.sample.foursquare.venue_id";
    private String venueId;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = new VenueActivityViewModel();
        viewModel.attach(this);

        bindView(R.layout.activity_venue);

        binding.webView.setWebChromeClient(new WebChromeClient());
        binding.webView.setWebViewClient(new WebViewClient());

        Intent intent = getIntent();
        venueId = intent.getStringExtra(VenueActivity.EXTRA_VENUE_ID);
    }

    @Override protected void onStart() {
        super.onStart();
        load(venueId);
    }

    @Override public void load(String venueId) {
        binding.webView.loadUrl(BuildConfig.URL_VENUE + venueId);
    }
}
