package com.sample.foursquare.ui;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sample.foursquare.R;
import com.sample.foursquare.databinding.VenueRowLayoutBinding;
import com.sample.foursquare.models.Item;
import com.sample.foursquare.ui.activities.VenueActivity;

import java.util.Collections;
import java.util.List;


/**
 * VenueAdapter
 * Created by fabricio on 22/04/2017.
 */

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.VenueViewHolder> {

    private List<Item> mItems = Collections.emptyList();
    private Context mContext;

    public VenueAdapter() { }

    @Override public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        VenueRowLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.venue_row_layout, parent, false);
        return new VenueViewHolder(binding);
    }

    @Override public void onBindViewHolder(VenueViewHolder holder, int position) {

        Item item = mItems.get(position);
        holder.update(item);
        holder.itemView.setOnClickListener(view -> {

            Intent intent = new Intent(mContext, VenueActivity.class);
            intent.putExtra(VenueActivity.EXTRA_VENUE_ID, item.getVenue().getId());
            mContext.startActivity(intent);

        });
    }

    @Override public int getItemCount() {
        return mItems.size();
    }

    public void setData(List<Item> list) {
        this.mItems = list;
        notifyDataSetChanged();
    }

    public void clear() {
        this.mItems = Collections.emptyList();
        notifyDataSetChanged();
    }

    public void addData(Item item) {
        this.mItems.add(item);
        notifyDataSetChanged();
    }

    //View Holder
    static class VenueViewHolder extends RecyclerView.ViewHolder {

        VenueRowLayoutBinding binding;

        VenueViewHolder(VenueRowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void update(Item item) {

            binding.setItem(item);
            binding.executePendingBindings();
        }
    }
}
