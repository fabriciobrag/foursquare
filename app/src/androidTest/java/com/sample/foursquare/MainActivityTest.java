package com.sample.foursquare;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.sample.foursquare.ui.activities.MainActivity;

import static android.support.test.espresso.action.ViewActions.click;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void setUp() throws Exception {
        rule.launchActivity(null);
    }

    @Test
    public void testLaunch() {
        Espresso.onView(ViewMatchers.withText(R.string.app_name)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    @Test
    public void testList() {

        //list displayed
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view)).check(ViewAssertions.matches((ViewMatchers.isDisplayed())));

    }
}
